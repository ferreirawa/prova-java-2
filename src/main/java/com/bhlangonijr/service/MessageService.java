package com.bhlangonijr.service;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageService.class);

    //Foi necessário criar uma variável global: List<Message> messages = new ArrayList<>().
    //Esta variável tem a função de armazenar as mensagens recebidas para posteriormente poderem ser consultadas.
    private List<Message> messages = new ArrayList<>();

    //Foi preciso criar o método abaixo para validar a mensagem.
    private boolean validateData(Message message){
        return message.getFrom().equals("me@earth");
    }

    /**
     * Validate and store this message into the database
     *
     * @param message
     * @return
     * @throws Exception
     */
     
    //Este método precisou ser incrementado.
    public Response send(Message message){
        Response response = new Response();

        response.setSuccess(validateData(message));

        //Se a mensagem passar na validação, então ela é amarzenada no banco.
        //Neste caso, a banco de dados é um ArrayList.
        if (response.getSuccess()){
            messages.add(message);
            response.setText("hello there - has been processed");
        } else{
            response.setText("Message cannot be sourced by martians!");
        }
        return response;
    }

    public List<Message> getAllMessages() {
        return messages;
    }

    //O método getById abaixo precisou ser implementado.
    //A finalidade deste método é localizar uma mensagem em um ArrayList a partir do ID da mensagem
    public Message getById(String id) {

        //Aqui utilizei o mecanismo Lambda que esta disponível no java a partir da versão 8.
        //Poderia também ter utilizado um laço for ou while para varrer a lista e retornar a mensagem com o id desejado, casou houvesse.
        List<Message> filtered = messages.stream().filter(m -> m.getId().equals(id)).collect(Collectors.toList());
        
        //Se a lista estiver vazia, então não há nenhuma mensagem com o id desejado. Retorna null.
        if (filtered.isEmpty()){
            return null;
        } else{
            //Caso contrário, retorne a mensagem. 
            return filtered.get(0);
        }
    }
}
